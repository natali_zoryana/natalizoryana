# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 18:35:23 2018

@author: Наталья
"""
alphabet_roman = {'I':1, 'V':5, 'X':10, 'L':50, \
                'C':100, 'D':500, 'M':1000}
alphabet_arab = {1: 'I', 2: 'II', 3: 'III', 4: 'IV', 5: 'V', 6: 'VI', 7: 'VII',\
                 8: 'VIII', 9: 'IX',
                 10: 'X', 20: 'XX', 30: 'XXX', 40: 'XL', 50: 'L', 60: 'LX',\
                 70: 'LXX', 80: 'LXXX', 90: 'XC',
                 100: 'C', 200: 'CC', 300: 'CCC', 400: 'CD', 500: 'D', \
                 600: 'DC', 700: 'DCC', 800: 'DCCC', 900: 'CM',
                 1000: 'M'}
wrong_combination = ['IIII', 'XXXX', 'CCCC', 'VV', 'LL',\
     'DD', 'IL', 'IC', 'ID', 'IM', 'VX',\
     'VL', 'VC', 'VD', 'VM', 'XD', 'XM', 'LC', \
     'LD', 'LM', 'DM', 'VIV']
number = input("Please, enter a number (roman/arabic)"\
              +" [arabic integer should be positive and less than 10000] \n: ")
try:
# if arabic number is positive and less than 10,000, we convert to roman number
    if int(number) > 0 and int(number) < 10000:
        digits = list(str(number))
        roman_number = ''
        if len(digits) == 4:
            roman_number += 'M'*int(digits[0])
            digits = digits[1:]
        for i, elem in enumerate(digits):
            if int(elem) != 0:
                roman_number += alphabet_arab[int(elem)*10**(len(digits)-1-i)]
        print("roman number = ", roman_number)
    else:
        print("not a positive arabic number less than 10,000")
except ValueError:
    number = number.upper()
# function for checking if the given number is a roman number
    def check_if_roman_number(roman_number):
# here we eliminate numbers which contain letters differ from roman
# numbers system
        for char in list(roman_number):
            if char not in alphabet_roman.keys():
                return False
# here we eliminate numbers which contain wrong combinations of letters from
# roman numbers system
        for elem in wrong_combination:
            if elem in roman_number:
                return False
        for i in range(len(roman_number)-1):
# here we eliminate numbers such as IXI, XLX and similar
            if alphabet_roman[roman_number[i]] < alphabet_roman[roman_number[i+1]]\
            and (i < len(roman_number)-2)\
            and alphabet_roman[roman_number[i]] == alphabet_roman[roman_number[i+2]]:
                return False
# here we eliminate numbers such as XXL, CCD, IIV, IXL, CDM
            elif (alphabet_roman[roman_number[i]] <= alphabet_roman[roman_number[i+1]])\
            and (i < len(roman_number)-2)\
            and alphabet_roman[roman_number[i]] < alphabet_roman[roman_number[i+2]]:
                return False
        return True
# if the given number is roman, we convert it to arabic
    if check_if_roman_number(number):
        arab_number = 0
        for i in range(len(number)-1):
# if smaller number goes before the given number then substract
            if alphabet_roman[number[i]] < alphabet_roman[number[i+1]]:
                arab_number -= alphabet_roman[number[i]]
# if bigger or equal number goes before the given number then add
            else:
                arab_number += alphabet_roman[number[i]]
        arab_number += alphabet_roman[number[-1]]
        print("arabic number = ", arab_number)
    else:
        print("not a roman number")
